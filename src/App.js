import React, { Component } from "react";
import Dashboard from "components/Dashboard";
import { Container } from 'react-bootstrap';

class App extends Component {
  render() {
    return (
      <Container>
        <Dashboard />
      </Container>
    );
  }
}

export default App;

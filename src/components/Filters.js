import React from 'react';
import { Form, Col } from 'react-bootstrap';

import './Filters.css';

const valuesToOptions = (data) => (
  data.map((datum, idx) => <option key={idx}>{datum}</option>)
);


export default function Filters(props) {
  return (
    <Form>
      <Form.Row>
        <Col xs={6}>
          <Form.Control
            placeholder="Search for..."
            onChange={props.handleInputChange}
            />
        </Col>
        <Col>
          <Form.Control
            as="select"
            size="sm"
            options={props.categories}
            onChange={props.onCategoryChange}
            >
            <option>Select Category...</option>
              {valuesToOptions(props.categories)}
              </Form.Control>
        </Col>
        <Col>
          <Form.Control
            as="select"
            size="sm"
            onChange={props.onThemeChange}
            >
            <option>Select Theme...</option>
            {valuesToOptions(props.themes)}
    </Form.Control>
      </Col>
      </Form.Row>
    </Form>
  );
}

import React from "react";
import { ListGroup } from 'react-bootstrap';

import './ReviewsList.css';

export default function ReviewsList(props) {
  if(props.reviews) {
    return (
      <ListGroup>
        <ListGroup.Item variant="dark" key={0}>{props.count} reviews found.</ListGroup.Item>

        {
          props.reviews.map((review) => (
            <ListGroup.Item key={review.id}>{review.comment}</ListGroup.Item>
          ))
        }
      </ListGroup>
    );
  }

  return <p>No comments found</p>;
}

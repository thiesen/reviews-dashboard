import React, { Component } from "react";
import Chart from 'components/Chart';
import Filters from "components/Filters";
import ReviewsList from "components/ReviewsList";

import { Col, Row } from 'react-bootstrap';

export default class Dashboard extends Component {
  state = {
    categories: [],
    themes: [],
    reviews: [],
    count: 0,
    category: null,
    theme: null,
  }

  componentDidMount() {
    this.mapSentimentToState();
  }


  getQuery = () => {
    let x = this.state.query || '*';

    if (this.state.category) {
      x += ' AND themes.category:' + this.state.category;
    }

    if (this.state.theme) {
      x += ' AND themes.theme:' + this.state.theme;
    }

    return escape(x);
  }

  mapSentimentToState() {
    fetch(`http://localhost/sentiment?query=${this.getQuery()}`)
      .then(res => res.json())
      .then((data) => {
        this.setState({
          reviews: data.reviews,
          count: data.count,
          categories: this.mapData(data.categories),
          themes: this.mapData(data.themes),
        });
      }).catch(console.log);
  }

  mapData = (data) => {
    if (data) {
      return data.map((s) => (
        {
          name: s.name,
          y: s.value * 100,
        }
      ));
    }

    return [];
  }

  handleInputChange = (event) => {
    this.setState(
      { query: event.target.value },
      () => { this.mapSentimentToState(); },
    );
  }

  handleCategoryFilter = (event) => {
    this.setState(
      { category: event.target.value },
      () => { this.mapSentimentToState(); },
    );
  }

  handleThemeFilter = (event) => {
    this.setState(
      { theme: event.target.value },
      () => { this.mapSentimentToState(); },
    );
  }

  render() {
    return (
      <div className="dashboard">
        <Row>
          <Filters
            handleInputChange={this.handleInputChange}
            onCategoryChange={this.handleCategoryFilter}
            onThemeChange={this.handleThemeFilter}
            categories={this.state.categories.map((category) => category.name)}
            themes={this.state.themes.map((theme) => theme.name)}
            />
        </Row>
        <Row>
          <Col>
            <Chart data={this.state.categories} title={'Sentiment by Category'} />
          </Col>
          <Col>
            <Chart data={this.state.themes} title={'Sentiment by Theme'} />
          </Col>
        </Row>
        <Row>
          <ReviewsList reviews={this.state.reviews} count={this.state.count}/>
        </Row>
      </div>
    );
  }
}

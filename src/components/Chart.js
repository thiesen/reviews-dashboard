import React from 'react'
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

const chartOptions = (title, data) => ({
  tooltip: { enabled: false },
  chart: {
    type: 'bar',
  },

  title: {
    text: title
  },

  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        inside: true
      }
    }
  },

  xAxis: {
    type: 'category',
    lineWidth: 0,
    tickWidth: 0,
  },

  yAxis: {
    title: {
      text: ''
    },
  },

  series: [{
    data: data,
    dataLabels: [{
      align: 'right',
      format: '{y:.1f}%'
    }],
    pointWidth: 30,
    showInLegend: false
  }]
});

export default function Chart(props) {
  return(
    <HighchartsReact
      highcharts={Highcharts}
      options={chartOptions(props.title, props.data)} />
  );
}
